<?php
session_start();
?>

<html>
    <head>
        <title>Gender selection</title>
    </head>
    <body>
        <fieldset>
            <legend>Select your gender</legend>
            <form action="store.php" method="POST">
                <?php
                if (isset($_SESSION["message"]) && !empty($_SESSION["message"])) {
                    echo ($_SESSION["message"]);
                    unset($_SESSION["message"]);
                }
                ?>
                <label>Add your gender :</label><br><br>
                <p1>Name :</p1>
                <input type="text" name="Name"><br><br>
                <p1>Gender :</p1>
                <select type="text" name="Gender">
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                 </select>
                <input type="submit" value="Add">
            </form>
        </fieldset>
    </body>
</html>